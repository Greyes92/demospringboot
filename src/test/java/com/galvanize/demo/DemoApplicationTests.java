package com.galvanize.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {
	private DemoApplication demo;

	@BeforeEach
	public void setUp(){
		demo = new DemoApplication();
	}

	@Test
	void testReturnNum() {
		int actual = demo.returnNum();
		int expected = 7;

		assertEquals(expected, actual);
	}

}
